package com.goryntsev.simpleweb.service;

import com.goryntsev.simpleweb.entity.User;

/**
 * @author goryntsev.y
 * @version 1.0
 */
public interface UserService {

    User getUser(String login);
}
